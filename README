Deployment Logger
=================

Fusionary's very own capstrano deployment logger.

Setup
=====
Run bundle install

copy config/settings.yml.example to config/settings.yml
copy config/mongoid.yml.example to config/mongoid.yml

Edit the config files, then deploy as usual (or run rackup)


Logging Deployments
===================

Add the following to your cap recipes:

after "deploy", "notify_tracker"
set :deployment_tracker_url, "http://your.tracker.host/create"
set :deployment_tracker_api_key, "yourapikey"

desc "post deployment info to tracker"
task :notify_tracker do
  tracker_url = fetch(:deployment_tracker_url, nil)
  api_key = fetch(:deployment_tracker_api_key, nil)

  unless tracker_url && api_key
    raise "Please set deployment tracker host and API key in deploy.rb"
  end

  gitconfig = File.join(ENV['HOME'], ".gitconfig")
  gitconfig_hash = {}
  if File.exist?(gitconfig)
    File.open(gitconfig, "r") do |f|
      f.readlines.each do |line|
        if match = line.match(/(.*)\=(.*)/)
          gitconfig_hash[match[1].strip] = match[2].strip
        end
      end
    end
  end
  username = gitconfig_hash["name"] || ENV['USER']
  email = gitconfig_hash["email"]
  from = source.next_revision(current_revision)
  log = %x{#{source.local.log(from)}}

  require "net/http"
  require "uri"
  tracker_host = URI.parse(tracker_url)
  post_data = {
    "api_key" => api_key,
    "deployment[remote_user]" => user,
    "deployment[deployed_by_name]" => username,
    "deployment[deployed_by_email]" => email,
    "deployment[application]" => application,
    "deployment[stage]" => ENV['STAGE'],
    "deployment[rev]" => real_revision[0, 6],
    "deployment[revision]" => revision,
    "deployment[changelog]" => log
  }

  begin
    response = Net::HTTP.post_form(tracker_host, post_data)
    if response.code != "201"
      raise RuntimeError
    end
  rescue
    puts "******** ERROR: Deployment was not tracked. ********"
  end
end
