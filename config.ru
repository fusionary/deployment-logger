require 'rubygems'
require 'bundler'

if ENV['RACK_ENV'] == 'production'
  log = File.new(File.join(File.dirname(__FILE__), 'log', 'tracker.log'), 'a')
  STDOUT.reopen(log)
  STDERR.reopen(log)
end

Bundler.require
require './deploy_tracker'
run DeployTracker
