/*
 * Application Javascript
 */

$(document).ready(function() {
    $('.changelog').condense();
    /*
     * Load more items
     */
    $('#display-more').click(function(e) {
	e.preventDefault();
	$('#loading-more-img').show();
	var lastObject = $('#deployment-items div.deployment-item:last').attr('data-number'),
	    btn = this;
	$('#since-field').val(lastObject);

	// TODO graceful slidedown and scroll
	$.get('/',
	      $('form').serializeArray(),
	      function(data) {
		  $('#deployment-items').append(data).fadeIn('slow');
		  $('#loading-more-img').hide();
	      });
    });

    $('#date-range-filter ul li a').click(function(e) {
	e.preventDefault();
	var $link = $(this),
	inputVal = $link.attr('id').replace(/date-range-/, '');
	if (inputVal == "all") inputVal = "";
	$('#date-field').val(inputVal);
	$('form').trigger('submit');
    });

    $('#stage-filter ul li a').click(function(e) {
	e.preventDefault();
	var $link = $(this),
	inputVal = $link.attr('id').replace(/stage-/, '');
	if (inputVal == "all") inputVal = "";
	$('#stage-field').val(inputVal);
	$('form').trigger('submit');
    });

    $('form').submit(function() {
	$('#since-field').val('');
    });
});
