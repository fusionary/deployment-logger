## Fusionary deployment tracker

## Data models
require File.join(File.dirname(__FILE__), 'deployment')

class DeployTracker < Sinatra::Base
  configure do
    config_path      = File.join(File.dirname(__FILE__), 'config')
    mongoid_config   = File.join(config_path, 'mongoid.yml')
    app_config       = File.join(config_path, 'settings.yml')
    mongoid_settings = YAML.load(ERB.new(File.new(mongoid_config).read).result)
    app_settings     = YAML.load(ERB.new(File.new(app_config).read).result)

    Mongoid.configure do |config|
      config.from_hash(mongoid_settings[ENV['RACK_ENV']])
    end

    set :public, File.join(File.dirname(__FILE__), 'public')

    env_settings = app_settings[ENV['RACK_ENV']]
    set :api_key, env_settings.fetch('api_key')
    set :user, env_settings.fetch('http_auth_user')
    set :pass, env_settings.fetch('http_auth_pass')
  end

  helpers do
    def auth
      @auth ||= Rack::Auth::Basic::Request.new(request.env)
    end

    def unauthorized!(realm="fusionary.com")
      headers 'WWW-Authenticate' => %(Basic realm="#{realm}")
      throw :halt, [ 401, 'Authorization Required' ]
    end

    def bad_request!
      throw :halt, [ 400, 'Bad Request' ]
    end

    def authorized?
      request.env['REMOTE_USER']
    end

    def authorize(username, password)
      [username, password] == [options.user, options.pass]
    end

    def require_administrative_privileges
      return if authorized?
      unauthorized! unless auth.provided?
      bad_request! unless auth.basic?
      unauthorized! unless authorize(*auth.credentials)
      request.env['REMOTE_USER'] = auth.username
    end
  end

  get "/" do
    require_administrative_privileges

    deployments = Deployment.all(:limit => 10).descending(:number)

    unless (q = params[:q]).blank?
      deployments = deployments.search(q)
    end

    unless (since = params[:since]).blank?
      deployments = deployments.where(:number.lt => params[:since])
    end

    unless (stage = params[:stage]).blank?
      deployments = deployments.where(:stage => params[:stage])
    end

    unless (date = params[:date]).blank?
      deployments = case date
                    when "this_week" then deployments.this_week
                    when "last_week" then deployments.last_week
                    when "month" then deployments.this_month
                    else deployments
                    end
    end

    @deployments = deployments

    if request.xhr?
      erb :deployment_items, :layout => false
    else
      erb :index
    end
  end

  # Post to API
  post "/create" do
    unless params[:api_key] == options.api_key
      halt 401, 'Not authorized'
    end
    @deployment = Deployment.create!(params[:deployment])
    halt 201, '/'
  end
end
