require 'digest/md5'
require File.join(File.dirname(__FILE__), 'sequence')

class Deployment
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Sequence

  field :number, :type=>Integer
  sequence :number
  field :remote_user
  field :deployed_by_name
  field :deployed_by_email
  field :application
  field :stage
  field :rev
  field :revision
  field :changelog

  index :number
  index :remote_user
  index :deployed_by
  index :application

  class << self
    def this_week
      criteria.where(:created_at.lt => Time.now).and(:created_at.gt => Time.now.beginning_of_week)
    end

    def last_week
      criteria.where(:created_at.lt => 1.week.ago.end_of_week).and(:created_at.gt => 1.week.ago.beginning_of_week)
    end

    def this_month
      criteria.where(:created_at.lt => Time.now).and(:created_at.gt => Time.now.beginning_of_month)
    end

    def search(q)
      criteria.where({"$or" => [{:deployed_by_name => /#{q}/i},
                                {:remote_user => /#{q}/i},
                                {:changelog => /#{q}/i},
                                {:deployed_by_name => /#{q}/i},
                                {:application => /#{q}/i}]})
    end

    def date_range(to, from)
      to = DateTime.parse(to) unless to.is_a?(DateTime)
      from = DateTime.parse(from) unless to.is_a?(DateTime)
      criteria.where(:created_at.lt => to).and(:created_at.gt => from)
    end
  end

  def gravatar_img
    gravatar_hash = Digest::MD5.hexdigest(self.deployed_by_email)
    "http://www.gravatar.com/avatar/#{gravatar_hash}?s=25&d=identicon"
  end
end
